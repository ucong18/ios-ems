//
//  Company.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit

class Company: NSObject {
    var id: Int?
    var nameCompany : String?
    var emailCompany: String?
    var addressCompany: String?
    var logoCompany:String?
    var password: String?
    var contactCompany: String?
    var dateOfBirth: String?
    var createdAt: Date?
    var updatedAt:Date?
    
    
    init?(name:String, email:String, password:String) {
        
        guard !name.isEmpty else {
            return nil
        }
        
        guard !email.isEmpty else {
            return nil
        }
        
        guard !password.isEmpty else {
            return nil
        }
        
        
        self.nameCompany = name
        self.emailCompany = email
        self.password = password
        
    }
}
