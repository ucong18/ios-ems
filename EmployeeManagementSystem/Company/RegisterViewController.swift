//
//  RegisterViewController.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterViewController: UIViewController {

    @IBOutlet weak var labelNameCompany: UITextField!
    
    @IBOutlet weak var labelEmailCompany: UITextField!
    @IBOutlet weak var labelPasswordCompany: UITextField!
    
    let URL_COMPANY_CREATE = "http://192.168.1.18:8080/api/v1/company/addCompany"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func goRegister(_ sender: Any) {
        let name = labelNameCompany.text
        let email = labelEmailCompany.text
        let password = labelPasswordCompany.text
        
        
        if(email!.isEmpty || password!.isEmpty || name!.isEmpty){
            displayAlertMessage(alertTitle: "Alert", userMessage: "All Fields is Required")
            return
        }
        
        
        if(password!.count < 6){
            displayAlertMessage(alertTitle: "Alert", userMessage: "Password Length 6")
            return
        }
        
        let parameters: Parameters = [
            "name_company" :name ?? nil!,
            "email" :email ?? nil!,
            "password" : password ?? nil!
        ]
        
        //making a post request
        Alamofire.request(URL_COMPANY_CREATE, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            
            if let result = response.result.value {
                let jsonData = result as! NSDictionary
                
                if(((jsonData.value(forKey: "message"))) as! String == "Login success"){
                    self.displayAlertMessage(alertTitle: "Success", userMessage: "Please Edit Your Company Profile")
                    
                    let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginView") as UIViewController
                    
                    self.present(viewController, animated: true, completion: nil)
                    
                }else{
                    self.displayAlertMessage(alertTitle: "Alert", userMessage: "Action Failed")
                    return
                }
            }
        }
        
    }
    
    func displayAlertMessage(alertTitle: String, userMessage:String){
        
        let alertController = UIAlertController(title: alertTitle, message:
            userMessage, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,
                                                handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func goToPageLogin(_ sender: Any) {
        let viewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginView") as UIViewController
        
        self.present(viewController, animated: true, completion: nil)
    }
    
}
