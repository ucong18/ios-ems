//
//  AddEmployeeViewController.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddEmployeeViewController: UIViewController {

    @IBOutlet weak var namaTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var foto: UIImageView!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var contactTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var dobTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var posittionTextField: UITextField!
    @IBOutlet weak var cvTextField: UITextField!
    @IBOutlet weak var departemenTextField: UITextField!
    
    private var datepicker:UIDatePicker?
    
    @IBOutlet weak var tblDropDown:UITableView!
    @IBOutlet weak var tblDropDownHC: NSLayoutConstraint!
    
    var idCompany:String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDatePicker()
        let id = UserDefaults.standard.integer(forKey: "idCompany")
        self.idCompany = String(id)
        // Do any additional setup after loading the view.
    }
    let URL_COMPANY_CREATE = "http://192.168.1.18:8080/api/v1/employee/addEmployee"
    
    
    @objc func viewTapped(gestureRecog:UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker:UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dobTextField.text = dateFormatter.string(from: datepicker!.date)
        view.endEditing(true)
    }
    
    func setupDatePicker(){
        datepicker = UIDatePicker()
        
        datepicker?.datePickerMode = .date
        datepicker?.addTarget(self,action:#selector(EditCompanyViewController.dateChanged(datePicker:)),for: .valueChanged)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(EditCompanyViewController.viewTapped(gestureRecog:)))
        
        view.addGestureRecognizer(tapGesture)
        
        dobTextField.inputView = datepicker
        
        //circulare image
        foto.layer.cornerRadius = foto.frame.size.width / 2
        foto.clipsToBounds = true
    }
    
    
    func displayAlertMessage(alertTitle: String, userMessage:String){
        
        let alertController = UIAlertController(title: alertTitle, message:
            userMessage, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,
                                                handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    @IBAction func btnCreateNewEmployee(_ sender: Any) {
        let name  = namaTextField.text
        let email = emailTextField.text
        let contact = contactTextField.text
        let address = addressTextField.text
        let dob = dobTextField.text
        let country = countryTextField.text
        
        
        if(email!.isEmpty || name!.isEmpty || contact!.isEmpty ){
            displayAlertMessage(alertTitle: "Alert", userMessage: "All Fields is Required")
            return
        }
        
        
        let parameters: Parameters = [
            "first_name" :name ?? nil!,
            "email" :email ?? nil!,
            "address":address ?? nil!,
            "country":country ?? nil!,
            "phone_number":contact ?? nil!,
            "dob" :dob ?? nil!,
            "status": "AKTIF",
            "company" :[
                "id": self.idCompany
            ]
            
        ]
        
        //making a post request
        Alamofire.request(URL_COMPANY_CREATE, method: .post, parameters: parameters,encoding: JSONEncoding.default, headers: nil).responseJSON {
            response in
            
            if(response.result.isSuccess){
                self.displayAlertMessage(alertTitle: "Success", userMessage: "Please Edit Your Company Profile")
                
            }else{
                self.displayAlertMessage(alertTitle: "Alert", userMessage: "Action Failed")
            }
        }
    }

}
