//
//  Employee.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit

class Employee: NSObject {
    var id: Int?
    var first_name : String?
    var lastName: String?
    var email: String?
    var cv:String?
    var gender:String?
    var photo: String?
    var phone_number: String?
    var address: String?
    var country: String?
    var startWork: String?
    var positition: String?
    var departemen:Departemen?
    var company:Company?
    var status: String?
    var createdAt: Date?
    var updatedAt:Date?
    
    
    init?(first_name:String, lastName:String, email:String) {
        
        guard !first_name.isEmpty else {
            return nil
        }
        
        guard !lastName.isEmpty else {
            return nil
        }
        
        guard !email.isEmpty else {
            return nil
        }
        
        // Initialize stored properties.
        self.first_name = first_name
        self.lastName = lastName
        self.email = email
        
    }
}
