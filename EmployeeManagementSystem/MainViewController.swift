//
//  ViewController.swift
//  EmployeeManagementSystem
//
//  Created by fernando simangunsong on 12/08/19.
//  Copyright © 2019 fernando simangunsong. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MainViewController : UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    
    var idemployees = [Int]()
    var name = [String]()
    var email = [String]()
    var departemen = [String]()
    var foto = [String]()
    let baseurl = "http://192.168.1.18:8080/api/v1/employee/findMyEmployee/"
    var idCompany:String? = nil
    
   
    @IBOutlet weak var tvJSON: UITableView!
    
    @IBOutlet weak var labelNamaPT: UILabel!
    
    
     let fotodefault:String = "default_foto_user.png"
    
    
    //    var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let status = UserDefaults.standard.bool(forKey: "userlogin")
        // navigate to protected page
        if(status == false){
            self.performSegue(withIdentifier: "loginView", sender: self)
        }
    }
    
    func setData(){
        
        let id = UserDefaults.standard.integer(forKey: "idCompany")
        self.idCompany = String(id)
        
        labelNamaPT?.text = UserDefaults.standard.string(forKey: "name_company")
        
        
        DispatchQueue.main.async(execute: {
            Alamofire.request(self.baseurl+self.idCompany!, method: .get,encoding: JSONEncoding.default, headers: nil).responseJSON {
                response in
                if let value = response.result.value {
                    
                    let json = JSON(value)
                    let count = json.count
                    if(count>0){
                        for index in (0...count-1).reversed() {
                            
                            let itemId = json[index]["id"].intValue
                            self.idemployees.append(itemId)
                            
                            let itemName = json[index]["first_name"].stringValue
                            self.name.append(itemName)
                            
                            let itemEmail = json[index]["email"].stringValue
                            self.email.append(itemEmail)
                            
                            let itemfoto = json[index]["photo"].stringValue
                            if(itemfoto.isEmpty){
                                self.foto.append(self.fotodefault)
                            }else{
                                self.foto.append(itemfoto)
                            }
                            
                            
                            let itemDep = json[index]["departemen"]["name_department"].stringValue
                            self.departemen.append(itemDep)
                        }
                    }
                    
                }
                self.tvJSON?.reloadData()
            }})
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        // Configure the cell...
        let cell:EmployeeTableViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! EmployeeTableViewCell
        
        if(self.name.count != 0){
            cell.nama.text = name[indexPath.row]
            cell.email.text = email[indexPath.row]
            cell.departemen.text = departemen[indexPath.row]
            
        }
        
        if(self.foto.count != 0){
            cell.foto.image = UIImage(named: foto[indexPath.row])
        }
        
        //circulare image
        cell.foto.layer.cornerRadius = cell.foto.frame.size.width / 2
        cell.foto.clipsToBounds = true
        
        return cell as EmployeeTableViewCell
    }
    

    //mengahapus row
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            //delete the row from data source
            self.name.remove(at: indexPath.row)
            self.email.remove(at: indexPath.row)
            self.departemen.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            let id_selected = String (idemployees[indexPath.row])
            let url = "http://192.168.1.18:8080/api/v1/employee/delete/"
            
            
            Alamofire.request(url+id_selected, method: .delete,encoding: JSONEncoding.default, headers: nil).responseJSON {
                response in
                DispatchQueue.main.async {
                    tableView.reloadData()
                }
                self.displayAlertMessage(alertTitle: "Success", userMessage: "Employee Deleted!")
            }
        }
    }
    
    
    
    func displayAlertMessage(alertTitle: String, userMessage:String){
        
        let alertController = UIAlertController(title: alertTitle, message:
            userMessage, preferredStyle: UIAlertController.Style.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default,
                                                handler: nil))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    //Edit action
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Create an option menu as an action sheet
        let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        // Add actions to the menu
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        optionMenu.addAction(cancelAction)
      
        
        let callActionHandler = { (action:UIAlertAction!) -> Void in
            let alertMessage = UIAlertController(title: "Service Unavailable", message: "Sorry, the call feature is not available yet.Please retry later.", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
        }
        
        let callAction = UIAlertAction(title: "Call " + "123-000-\(indexPath.row)", style: UIAlertAction.Style.default, handler: callActionHandler)
        optionMenu.addAction(callAction)
        
        let isVisitedAction = UIAlertAction(title: "I've been here", style: .default, handler: {
            (action:UIAlertAction!) -> Void in
            
            let cell = tableView.cellForRow(at: indexPath)
            cell?.accessoryType = .checkmark
            //self.restaurantIsVisited[indexPath.row] = true
        })
        
        optionMenu.addAction(isVisitedAction)
        
        
        if let popoverPresentationController = optionMenu.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            //            popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            
            popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.size.width/2, y: self.view.bounds.size.height/2, width: 1, height: 1);
            
            
        }
        
        self.present(optionMenu, animated: true, completion: nil)
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editEmployee"{
            //
        }
    }
    
//     func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
//        if segue.identifier == "editEmployee" {
//            if let indexPath = tableView.indexPathForSelectedRow() {
//                let destinationController = segue.destination as! EditEmployeeViewController
//                destinationController.restaurant = [indexPath.row]
//            }
//        }
//    }
    
    @IBAction func goLogout(_ sender: Any) {
        
        UserDefaults.standard.set(false, forKey: "userlogin")
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        
        let status = UserDefaults.standard.bool(forKey: "userlogin")
        // navigate to protected page
        if(status == false){
            self.performSegue(withIdentifier: "loginView", sender: self)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

